#!/bin/sh

if [[ -z "$HOSTNAME_FQDN" ]]; then
  export HOSTNAME_FQDN="${NAMESPACE}.web.cern.ch"
fi

# Replace hostname in shibboleth2.xml template
envsubst < /shibboleth2.xml > /etc/shibboleth/shibboleth2.xml

# If there are files in /tmp/configmap that are not empty
# (overriden by a ConfigMap) copy them to /etc/shibboleth
if [ -n "$(ls -A /tmp/configmap)" ]
then
  for f in /tmp/configmap/*
  do
    if [ -s $f ]
    then
      cp /tmp/configmap/* /etc/shibboleth/
    fi
  done
fi

# in case of shibd container restart, we need to remove the socket
# so shibd can start again
rm -f /run/shibboleth/shibd.sock

exec /usr/sbin/shibd -F -c /etc/shibboleth/shibboleth2.xml
